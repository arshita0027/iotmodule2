## **IoT PROTOCOLS**

#### **1.4-20 mA Current Loop**
  - 4-20 mA loop became a standard in process industry, replacing 3-15 psi air compression standard.
  - This works on the fact the current in a loop is constant irrespective of value of resistors. It uses the *Ohms Law: V= IxR*

  ![](https://www.rapidtables.com/electric/ohm's%20law/basic%20circuit.PNG)
  - *Components of 4-20mA current loop*
     1. Sensors: Device that measures the process variable.
     2. Transmitter: It converts the measurement taken by sensor into equivalent current signal.(4mA is equivalent to zero value of process variable)
     3. Power Source: It is required to generate the current signal.
     4. Loop : It refers to the wire that connects the transmitter to receiver and power source. The material of the wire is itself a source of resistance so, care must be taken to select wire with least resistance.
     5. Receiver: The current signal is translated back into units that is easily understood by operator. Digital displays, actuators and valves are connected to the receiver.  

  ![](https://4.bp.blogspot.com/-z_soG73tNEc/XFmRybzrrdI/AAAAAAAAAr4/VHWU3DR67MUhHZMj5H7TPTbwRScwEDH3QCLcBGAs/s1600/Compontent%2Bthat%2Bmake%2Bup%2B4-20%2BmA%2Bloop.jpg)

  - *Pros and Cons of 4-20mA current loop*

  | Pros | Cons |
  | ---- | ---- |
  | 4-20mA is used by many industries | It can transmit only one particular process signal | 
  | It is easiest to connect and configure | For measuring more than one process variable, multiple loops must be created |
  | Less sensitive to background electrical noise | Loops must be properly insulated or else cause problems |
  | Simple to detect fault in system( 4mA is 0%) | Complexity increases exponentially with number of loops |
  | Requires less connections than other signal, thus reducing initial setup costs |  |

#### **2.Modbus**
  - Modbus is a communication protocol used with Programmable Logic Controllers (PLCs). It is used in IoT as a local interface to manage devices.
  - It uses the master-slave method in transmitting information. **Master** is the device sending info and **Slave** is the device requesting info.
  - In a modbus 247 slaves can be connected to 1 master, each slave having a unique address from 1-247.
  - Communication betwwen master and slave happen as a function code. The function code decides an action to be performed and slave then response as the function code is received.

   ![](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQN-67fE58Ww5iHeQYf9PrMSpIC7TDPoJFvpA&usqp=CAU)

  - Modbus can be used with two interfaces:
     1. ***Modbus RTU(Remote Terminal Unit): RS485***
         - Serial transmission standard like UART
         - Can put several RS485 devices on same bus
         - It is not directly compatible. Connect RS485 to USB for signals to go through.

     2. ***Modbus TCP/IP: Ethernet***
         - In general sense, modbus TCP/IP is Modbus RTU that has TCp interface and runs on Ethernet.
         - It is fully compatible with already installed Ethernet infrastructure (Cables, Hub, Switches, Devices)

#### **3.OPCUA**
  - OPC UA  is a client-server or pub-sub(publish-subscribe) based communication protocol   
  - Service based Architecture   
  - In  a client-server system, the server waits for the clients requests and responds accordingly   
  - It can also works as a pub-sub systems where client subscribes for the topics they want   
  - All the data transmission in OPC UA depends upon the client     
  - *Unified Architecture:*   
    - Data modelling and transport mechanisms are two basic parts
    - Platform independent
    - Use of Data structures to manage information and send it to cloud
    - Supports Object Oriented Programming   
  - OPC UA provides security in terms of *Symmetric and Asymmetric encryption* to protect confidentiality of communication:
    - Symmetric encryption for securing communication between OPC UA apps
    - Asymmetric encryption for key agreement (use of different key for encryption and decryption)
  - *Advantages of OPC UA:*   
    1. High performance
    2. High scalability
    3. Internet and Firewall
    4. Platform independent
    5. OOPs features 

  ![](https://assets.new.siemens.com/siemens/assets/api/uuid:8af017fa59db28e3383f12217f8819be1898b3bc/width:1125/quality:high/opc-ua-integration.jpg)

**OPC Server:**   
  - Implements OPC standard and provides interfaces to the outside world  
  - Manufacturer of hardware provides OPC server to allow standardized access  
  - It can provided as an stand-alone software, machine controller or embedded OPC server on the device   


**OPC Client:**   
  - OPC clients are applications that depend on data exchange with industrial systems  
  - OPC clients can access OPC server interfaces and exchange data with servers  


**OPC Protocols:**
  - OPC protocols are self-reliant and independent  
  - HDA(Historical Data Access): Access historical data and events through application like SCADA,etc.
  - DA(Data Access): This is used for fetching data (information containing name, value, timestamp, quality) from control systems
  - AE(Alarms and Events): describes behaviour of an OPC server and notify client about alarm conditions. This is one-time subscription based service with no storage.

## **CLOUD PROTOCOLS**
 - Cloud IoT core supports two protocols for device connection and communication- MQTT and HTTP.
 - Devices connect with cloud across 'bridge'- MQTT bridge or HTTP bridge.

#### **1.MQTT (Message Queuing Telemetry Transport)**

 - A light weight pb-sub system to publish and receive information as a client.
 - Designed for constrained devices with low bandwidth.
 - *MQTT Broker*: Server that connects various devices in MQTT. It receives and distributes messages to clients
 - *MQTT Client*:Any device that connects to MQTT Broker over a network. It can be a basic sensor or microcontrollers
 - *Topics*: Address where to publish message. It is denoted by strings seperated by backslash, each slash denoting topic level.
 - *Publisher*: MQTT client sending message to broker is called Publisher.
 - *Subscriber*: MQTT client receiving message from broker are called Subscriber
 - *Advantages:*
    1. Simultaneous connection between several devices.
    2. Any type of data can be transported( text or binary).Only condition is receiving client should be able to interpret the data.
    3. Quality of Service (QoS) feature to guarantee delivery of messages.
    4. Use in IoT systems: send command to control units and read(/publish) data from sensor clients. 

![](https://lh3.googleusercontent.com/proxy/9SEZdl_16W6smNmNrSJNmF-pusxri6JXjqIH-6AcFue3EPpXVNYMR7uz0tROHYFaWjLdh5b8SsLzfj9sL4PUUvpgPvdsidjIA4Vaqg)  

#### **2.HTTP( Hyper Text Transfer Protocol)**
  - HTTP is a connection less protocol. It is a bridge between requests and receive responses.

  ![](https://static.javatpoint.com/servletpages/servletterminology/images/servlet-http4.png)
  
  - **HTTP Request**
    - It is send from client to server.
    - Request line contains three parts: (1) Request line  (2) HTTP headers  (3) message
    - Different methods for request: (1)GET (2)DELETE (3)POST (4)PUT/PATCH
    - Example:   
    ` DELETE /echo/delete/json HTTP/1.1 `   
    ` Authorization: Bearer mt0dgHmLJMVQhvjpNXDyA83vA_PxH23Y `   
    ` Accept: application/json `   
    ` Content-Type: application/json `   
    ` Content-Length: 19 `   
    ` Host: reqbin.com `   

  - **HTTP Response**
    - It send from server to client
    - Response contains three parts: (1) Response line  (2) HTTP headers  (3) message
    - Server sends different status code depending upon situation.
    - Example:   
    ` HTTP/1.1 200 OK `   
    ` Content-Length: 19 `   
    ` Content-Type: application/json `   
    ` {"success":"true"} `   

  ![](https://i1.wp.com/csharpcorner.mindcrackerinc.netdna-cdn.com/article/create-api-with-asp-net-core-day-three-working-with-http-status-codes-in-asp/Images/image002.jpg)





  




