## **Basic Electronincs for Embedded**

#### 1.Analog and Digital Signals
 - Classification of signals based on output values:
     - *Analog* - Values in continuous range   
     - *Digital* - Discrete values in output. Most of modern technology use digital signals in 0(power off) and 1(power on)   

 - Difference between analog and digital-      

  | Analog | Digital |
  | ------ | ------ |
  | Analog signals represent physical measurements | Digital signals represent digital information |
  | Dentoed by sine waves | Dentoed by square waves |
  | Range for output values is infinity | Output can take only finite values |
  | Analog instruments use scale. So, they can give considerable error | Digital instruments rarely produce errors |
  | Produce too much noise | Do not produce noise |
  | Ex- voice in air, analog voltmeters, FM radio signals | Ex-Computers, digital multimeters |
  | Application- Temperature sensors,LDR sensors | Application in DVDs, CDs |   
 - Analog and Digital signals can be converted from one form to another with the help of Analog-to-Digital converters (ADC) and Digital-to-Analog converters (DAC)


#### 2.Sensors and Actuators  
 - *Transducer*: Device that converts one form of energy into different form.   
    Ex-Bulb, Fan, Thermometer.   
 - *Sensors* : Sensors are subset of Transducer that convert any form of energy into electrical signals.   
    Ex- Proximity sensor, Water level sensor, Microphone   
 - *Actuators* : Actuators are subset of transducer that convert electricl signals into any form of energy.   
    Ex- Electric motor, Relays, Hydraulic systems   
 - **Imp** : Sensors and transducers are complimentary of each other.In an IoT system, sensors are used in the input side whereas Actuators are used in output side.   

![](https://www.avsystem.com/media/top_sensor_types_used_in_iot-02.png)


#### 3.Microprocessors and Microcontrollers   

 - Both microprocessors and microcontrollers both have CPU, timers and I/O pins and are widely used depending upon requirements.
 - Taking an example of Arduino and Rasberry Pi , we compare the features of microcontroller and microprocessor

 |   | Arduino | Rasberry Pi( RPi ) |
 | --- | ----- | ----- |  
 | OS | None | Linux or others |
 | Clock Speed | 16MHz | 700MHz | 
 | RAM | 2KB | 512 MB |
 |Register width | 8-bit | 32-bit |
 | GPIO | 20 | 8 |
 |Max I/O current | 40mA | 5-10 mA |
 | Power | 175 mW | 700mW |

 - We can conclude that microcontroller have strong I/O capability and drive external hardware directly. Microprocessor, on the other hand have strong processing capabilities.
 - For our projects involving sensors, motors, LCD Arduino is suited. However, if project includes videos, camera, complex math and graphical interfaces Rpi is suited best.

![](https://blog.leapmotion.com/wp-content/uploads/2015/06/arduino_raspberrypi.jpg)

#### 4.More on Rasberry Pi

 - RPi is a microprocessor, more like a mini computer as its features are more similar to computer than a microprocessor.
 - *Features:*
   - RPi uses ARM processor (System on chip)
   - Can use RPi OS (default settings) or load linux and other OS
   - No storage
   - Connect sensors and Actuators
   - Connect multiple RPi's together
   - Connect shields (add-on functionalities)
   - Can use WiFi, Bluetooth and ethernet connections
 - Interfaces are connection between sensors and microprocessor.
 - *RPi Interfaces:*
    - GPIO
    - UART
    - SPI
    - PWM
    - I2C

#### 5.Serial and Parallel communication
 - Serial communnication data in transferred one bit at a time. In parallel communication, multiple parallel links are used ro transmit bits simultaneously
 ![](https://miro.medium.com/max/1103/0*Hi44DwHAAkm7Jh_q.gif) 

 - In RPi, GPIO is an example of parallel interface. I2C, UART, SPI are examples of serial interfaces.


