### **Electrical Board Safety Instructions**

Before we start handling devices in an IoT system, it is important to take all safety measures.Following are Do's and Dont's mentioned for an electrical board

#### 1.Power Supply 

 |  | Do's | Dont's |
 | --- | --- | --- |
 | 1. | Output voltage matches input voltage of power supply | Do not connect power supply without checking power rating |
 | 2. | Before turning on power, make sure all devices are connected properly | Never connect higher O/P to lower I/P |
 | 3. | Replace cracked or frayed connectors | Do not force connector into power socket |

#### 2.Handling 

 |  | Do's | Dont's |
 | --- | --- | --- |
 | 1. | Treat every device like it's energised, even if it does not look like it is not plugged in or operational | Do not handle the board when power is ON. |
 | 2. | Keep the board on flat stable surface (wooden table) | Never touch electical equipments when any part of body is wet |
 | 3. | Unplug the device before performing any operation | Do not touch any kind of metal to the development board |
 | 4. | If board becomes too hot, cool it with external USB fan | NEVER touch the board with wet hands |

#### 3.GPIO (General Purpose I/O)

 |  | Do's | Dont's |
 | --- | --- | --- |
 | 1. | Find whether the board runs on 3.3V or 5V | Never connect anything greater than 5V |
 | 2. | Always connect LEDs (or sensors) using appropriate resistors | Avoid making connections when the board is running |
 | 3. | To use 5V peripherals with 3.3V board, we require logic level converter | Do not connect motor directly, use a transistor to drive it |

#### **Guidelines on using Interfaces**
 ##### *1.UART (Universal Asynchronous Receiver- Transmitter)* 
 - Properly connect flow control signals when routing UART signals through FPGAs.
 - For connecting two devices, connect Tx pin to Rx pin of other device. Similarly, Rx pin to Tx pin of other device.
 - USB to TTL connection does not require a protection circuit.
 - For sensor interfacing UART requires protection circuit.
 - If devices are of different voltage ratings (3.3V and 5V) use level shifting mechanism (voltage divider).

 ##### *2.I2C ( Inter-Integrated Circuit)*
  - Ensure that pull-up registers are added to protect SDL and SDA lines when interfacing with sensors.
  - In case of inbuilt pull-up register in board, no need of external circuit.
  - If using bread board, use pull-up resistors in range of 2.2- 4k ohm.
  - I2C protcol can interfce max. 128 devices that can communicate with master unit as well as slave devices. 

##### *3.SPI(Serial Peripheral Interface)*
 - SPI in development board in push-pull mode does not require any protection circuit.
 - In case of disturbance caused by other slave devices ( they can 'hear' and 'respond' to master) use a protection circuit.
 - For protection circuit, use pull resistors on each Slave Select Line (SSL)
 - Resistor values in range 1-10k ohm. Generally, 4.7k ohm is used.
 

