/*Solution for problem 2
  topic: Reading data from the RISHI Meter 3430 using Shunya Interfaces modbus API's.

  *Data required from the device-Current, Voltage, Power, Active energy, Reactive energy, Apparent energy and THD of voltage 
  *Meter connection through RS485 modbus protocol to a UART device node.

  @arshita0027
*/

/*---Standards---*/
#include <stdio.h> // for Standard Input and Output
#include <stdlib.h> // for functions involving memory allocation, process control,conversions and others
#include <string.h> // for manipulating C strings and arrays

/*---Shunya interfaces APIs---*/
#include <shunyaInterfaces.h> // program embedded boards to talk to PLC's, sensors and Cloud

/*---directory stream header file---*/
#include <dirent.h> // format of directory entries

add= opendir("/dev/ttyAMA0");

int main(void)
{
shunyaInterfacesSetup();

//modbus TCP/IP configuration
//user enter their modbus data
"modbus-tcp":{
"type": "tcp",
"ipaddr": " ",
"port": " "}


//modbus connect read/write data
modbusObj add = newModbus("modbus-tcp"); // for creating new instance
modbusTcpConnect(&add); // connect to modbus as per configs

// reading data of different parameters
float ac_current = modbusTcpRead(&add, 30007); //ac-current
float voltage = modbusTcpRead(&add, 30001);// voltage
float power = modbusTcpRead(&add, 30013);// power
float active_energy = modbusTcpRead(&add, 30147);//active_energy
float reactive_energy = modbusTcpRead(&add, 30151);//reactive energy
float apparent_energy = modbusTcpRead(&add, 30081);// apparent energy
float THD_of_voltage = modbusTcpRead(&add, 30219);// thd of voltage

return 0;
}
