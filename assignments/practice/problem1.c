/*Solution for problem 1 
 topic: C code to send heartbeat message to user using MQTT broker
   
*Heart beat messages are sent to the cloud to say that the device is active
*The messages are sent in JSON format
*Messages are sent to the AWS IoT core through MQTT protocol
 
 @arshita0027
*/

/*----Standards----*/
#include<stdio.h>  //standard input-output operations
#include<stdlib.h> // for memory allocation, process control and others
#include<string.h> //manipulating arrays and strings in C

/*---network header files---*/
#include<netdb.h> //for network interface operations
#include<ifaddrs.h> //to get network interface addresses

/*---shunya interface APIs---*/
#include<shunyaInterfaces.h> //to program embedded boards to talk with sensors, PLCs and cloud

/*---time header file---*/
#include<time.h> //to manipulate date and time information

/*---MQTT protocol header file---*/
#include<MQTTClient.h> //contains APIs to send/receive MQTT data




void getdeviceid(void)  
{
/* This function block fetches ID of the device which is stored in "/etc/shunya/deviceid"
 * The UNIX timestamp fetched through API from time.h header file.
 * The event type will be "heartbeat" message.
 * We fetch the IP address of the network at which our device is connected.
 * All data is stored in JSON format
 */

FILE *idfile;  // Interface and type library definitions
char id[255];  
idfile = fopen("/etc/shunya/deviceid", "r");  // Open a file to perform various operations 
while(fscanf(idfile, "%s", id)!=EOF)
{
    printf("%s ", id );  // Print device ID
}
// Fetching timestamp
int time_stamp = (gettimeofday(2));  // Get and set the time as well as a timezone.
void checkIPbuffer(char *ip)  // Fetching IP address
{
if (NULL == ip)
    {
    perror("inet_ntoa");  // Do some error checking 
    exit(1);}
    }  // Convert IP string to decimal format
char *ip;  
ip = inet_ntoa(*((struct in_addr*) host_entry->h_addr_list[0]));  // Convert value into IP string
}

void AWS_configuration(void)  
{
/* configuring connections with AWS IoT core
* Endpoint is the URL at which user network will connect.
* Default port value 8833
* Certificate directory is directory of the certificates of AWS
* root certificate is a public key certificate, client certificate is used by client systems to make authenticated requests
* Private key is stored on user's device and is used to decrypt data.
* Client ID is a special ID given to an user.
*/
"user": {
"endpoint": " ",  
"port": 8883,  
"certificate dir": "/home/shunya/.cert/aws/",  
"root certificate": " ",  
"client certificate": " ",  
"private key": " ",  
"client ID": " "}  
}


void DataSend()  
{
    // sending a JSON message to an AWS IoT core via MQTT broker.
MQTTClient_message heartbeat = MQTTClient_message_initializer; //initializes message to be sent on MQTT broker
awsObj user = newAws("user"); //creates new instance with AWS
awsConnectMqtt(&user); //connects to the MQTT broker of AWS IoT core
heartbeat.payload("device":{"deviceId": ID,"timestamp": TSP,"eventType": "heartbeat","ipAddress": ip}.getBytes()); //creates payload of the message
awsPublishMqtt(&user, "device/heartbeat", "%s" ,heartbeat); //publishes a payload on AWS MQTT broker
awsDisconnectMqtt(&user); //releases connection with MQTT broker
}

int main (void)
{
//main function calls out all the above defined function in order to complete the whole process
shunyaInterfacesSetup () ;//initiate shunya interfaces
devicedata();
AWS_config();
send_data(); 
}





